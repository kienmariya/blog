import React from "react";

import styled from "styled-components";

const HeaderContainer = styled.div`
  width: 90%;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
`;

export default HeaderContainer;
