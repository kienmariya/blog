import React from "react";

import styled from "styled-components";

const PortfolioItemContainer = styled.div`
  width: 650px;
`;

export default PortfolioItemContainer;
