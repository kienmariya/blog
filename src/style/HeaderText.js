import React from "react";

import styled from "styled-components";

const f = () => props => console.log(props)();
const HeaderText = styled.h1`
  color: #484b4b;
  display: flex;
  justify-content: center;
  cursor: pointer;
`;

export default HeaderText;
