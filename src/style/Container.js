import React from "react";

import styled from "styled-components";

const Container = styled.div`
  width: 75%;
  margin: 0 auto;
  overflow: hidden;
`;

export default Container;
