import React from "react";

import styled from "styled-components";

const HeaderTextWrap = styled.div`
  display: flex;
  align-items: center;
`;

export default HeaderTextWrap;
