import React from "react";

import styled from "styled-components";

const PortfolioItemImg = styled.img`
  width: 300px;
  height: 220px;
`;

export default PortfolioItemImg;
