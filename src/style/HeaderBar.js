import React from "react";

import styled from "styled-components";

const HeaderBar = styled.div`
  background-color: #b7d7d8;
  display: flex;
`;

export default HeaderBar;
