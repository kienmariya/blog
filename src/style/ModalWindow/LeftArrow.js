import React from "react";

import styled from "styled-components";

const LeftArrow = styled.div`
  width: 0;
  height: 0;
  border-right: 100px solid #000;
  border-top: 50px solid transparent;
  border-bottom: 50px solid transparent;
  cursor: pointer;
  align-self: center;
`;

export default LeftArrow;
