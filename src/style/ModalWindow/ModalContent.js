import React from "react";

import styled from "styled-components";

const ModalContent = styled.div`
  width: 1200px;
  position: relative;
  margin: 5% auto;
  background:  background: rgba(0, 0, 0, 1);
`;

export default ModalContent;
