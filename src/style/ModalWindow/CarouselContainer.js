import React from "react";

import styled from "styled-components";

const CarouselContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export default CarouselContainer;
