import React from "react";

import styled from "styled-components";
const HeaderTextWrap = styled.div`
  width: 480px;
  :nth-child(${props => props.selectItem})::after {
    content: "";
    display: block;
    height: 2px;
    width: 450px;
    margin-bottom: 15px;
    background-color: #007369;
  }
`;

export default HeaderTextWrap;
