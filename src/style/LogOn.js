import React from "react";
import styled from "styled-components";
const HeaderTextWrap = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  :focus {
    outline: none;
  }
`;

export default HeaderTextWrap;
