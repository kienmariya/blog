import React from "react";

import styled from "styled-components";

const Container = styled.div`
width: 80%;
margin: 0 auto;
display: flex;
justify-content: space-between;
`;

export default Container;
