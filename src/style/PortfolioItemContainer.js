import React from "react";

import styled from "styled-components";

const PortfolioItemContainer = styled.div`
  display: flex;
  margin-top: 40px;
  justify-content: space-between;
`;

export default PortfolioItemContainer;
