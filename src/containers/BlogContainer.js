import React from "react";
import { connect } from "react-redux";
import * as selectors from "../ducks/content-duck/Selectors";
import * as actions from "../ducks/content-duck/Actions";
import { withRouter } from "react-router-dom";

import ArticlesList from "../components/ArticlesList";


class BlogContainer extends React.Component {
    componentDidMount = () => {
        this.props.fetchBlogRequest();
    }
    handleAddArticleClick = () => {
        this.props.history.push("/addarticle");
    }
    render() {
        return (
            <div>
                <button onClick={this.handleAddArticleClick}>Добавить статью</button>
                <ArticlesList articles={this.props.articles} />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    articles: selectors.selectArticles(state)
});

const mapDispatchToProps = {
    fetchBlogRequest: actions.fetchBlogRequest
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(BlogContainer));
