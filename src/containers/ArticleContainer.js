import React from "react";
import { Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as selectors from "../ducks/content-duck/Selectors";
import * as actions from "../ducks/content-duck/Actions";

class ArticleContainer extends React.Component {
  componentDidMount = () => {
    const { id } = this.props.match.params;
    this.props.fetchArticleRequest(id);
  };
  render() {
    return <div dangerouslySetInnerHTML={{ __html: this.props.article }} />;
  }
}

const mapStateToProps = state => ({
  article: selectors.selectArticle(state)
});

const mapDispatchToProps = {
  fetchArticleRequest: actions.fetchArticleRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ArticleContainer);
