import React from "react";

import AddArticle from "../components/AddArticle";

class AddArticleContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      articleName: "",
      arriclePart: "",
      articleBody: "",
      articleDate: ""
    };
  }
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSubmit = event => {
    event.preventDefault();
  };
  render() {
    return (
      <AddArticle
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

export default AddArticleContainer;
