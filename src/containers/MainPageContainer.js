import React from "react";
import { Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as selectors from "../ducks/content-duck/Selectors";

import PortfolioContainer from "./PortfolioContainer";
import BlogContainer from "./BlogContainer";
import Container from "../style/Container";
class MainPageContainer extends React.Component {
  render() {
    return (
      <Container>
        {this.props.content === "portfolio" ? (
          <PortfolioContainer />
        ) : (
            <BlogContainer />
          )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  content: selectors.selectContent(state)
});

export default connect(
  mapStateToProps,
  null
)(MainPageContainer);
