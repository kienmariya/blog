import React from "react";
// import { Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import * as selectors from "../ducks/content-duck/Selectors";
import * as actions from "../ducks/content-duck/Actions";
import axios from "axios";

import PortfolioItem from "../components/PortfolioItem";
class PortfolioContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: []
    };
  }
  componentDidMount = () => {
    this.props.fetchPortfolioRequest();
  };
  render() {
    return this.props.projects.map((el, key) => {
      return <PortfolioItem key={key} data={el} />;
    });
  }
}

const mapStateToProps = state => ({
  projects: selectors.selectPortfolio(state)
});

const mapDispatchToProps = {
  fetchPortfolioRequest: actions.fetchPortfolioRequest
};

export default connect(mapStateToProps, mapDispatchToProps)(PortfolioContainer)



