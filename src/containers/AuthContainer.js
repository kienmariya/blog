import React from "react";
import { Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as selectors from "../ducks/auth-duck/Selectors";
import * as actions from "../ducks/auth-duck/Actions";

import axios from "axios";
class AuthContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: "",
      password: ""
    };
  }
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSubmit = event => {
    event.preventDefault();
    // this.props.authSuccess(true);
    this.props.authRequest(this.state.login, this.state.password);
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit} action="">
        <input type="text" name="login" onChange={this.handleChange} />
        <input type="text" name="password" onChange={this.handleChange} />
        <button onClick={this.handleSubmit}>Войти</button>
      </form>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  authRequest: actions.authRequest,
  authSuccess: actions.authSuccess
};

export default connect(
  null,
  mapDispatchToProps
)(AuthContainer);
