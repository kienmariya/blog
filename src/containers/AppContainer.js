import React from "react";
import { Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import ArticlesList from "../components/ArticlesList";
import AddArticleContainer from "./AddArticleContainer";
import Header from "../components/Header";
import MainPageContainer from "./MainPageContainer";
import ArticleContainer from "./ArticleContainer";
import AuthContainer from "./AuthContainer";
// import * as actions from "../ducks/auth-duck/Actions";
// import * as selectors from "../ducks/auth-duck/Selectors"

class AppContainer extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Route exact path="/" component={MainPageContainer} />
        <Route path="/addarticle" component={AddArticleContainer} />
        <Route path="/article/:id" component={ArticleContainer} />
        <Route path="/auth" component={AuthContainer} />
      </div>
    );
  }
}

export default AppContainer;

// const mapStateToProps = state => ({
//   username: selectors.selectUsername(state),
//   isAuth: selectors.selectAuthStatus(state)
// });

// const mapDispatchToProps = {
//   logOut: actions.logOut
// };

// export default withRouter(
//   connect(mapStateToProps, mapDispatchToProps)(AppContainer)
// );
