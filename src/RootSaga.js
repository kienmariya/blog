import { all, call, fork } from "redux-saga/effects";
import { contentRootSaga } from "./ducks/content-duck/Saga";
import { authRootSaga } from "./ducks/auth-duck/Saga";

export default function* rootSaga() {
  yield all([fork(contentRootSaga), fork(authRootSaga)]);
}
