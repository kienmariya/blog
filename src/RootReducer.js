import { combineReducers } from "redux";
import {
  ConnectedRouter,
  routerReducer,
  routerMiddleware,
  push
} from "react-router-redux";

// import { events } from "./ducks/events-duck/Reducer";
// import { auth } from "./ducks/auth-duck/Reducer"
import { content } from "./ducks/content-duck/Reducer";
import { auth } from "./ducks/auth-duck/Reducer";
export const rootReducer = combineReducers({
  content,
  auth,
  router: routerReducer
});
