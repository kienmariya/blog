import axios from "axios";
import config from "./config";

let instance = null;

export default class XHRProvider {
  constructor() {
    if (instance !== null) {
      return instance;
    }
    instance = axios.create({
      baseURL: config.uri,
      headers: {
        "Content-Type": "application/json"
      }
    });
  }

  get = path => instance.get(path).then(response => response.data);

  post = (path, body) =>
    instance.post(path, body).then(response => response.data);
}
