export const selectContent = state => state.content.content;

export const selectPortfolio = state => state.content.portfolio;

export const selectArticles = state => state.content.articles;

export const selectId = state => state.content.articleId;

export const selectArticle = state => state.content.article;