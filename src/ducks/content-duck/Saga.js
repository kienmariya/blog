import { put, takeLatest, all, call, select } from "redux-saga/effects";
import { push } from "react-router-redux";
import XHRProvider from "../../XHRProvider";
import * as types from "./ActionTypes";
import * as actions from "./Actions";
import * as selectors from "./Selectors";
const xhr = new XHRProvider();

function* fetchBlogSaga() {
  try {
    const response = yield call(xhr.get, "/blog");
    if (response) {
      yield put(actions.fetchBlog(response));
    }
  } catch (error) {
    console.log("/blog", error);
  }
}

function* fetchPortfolioSaga() {
  try {
    const response = yield call(xhr.get, "/portfolio");
    if (response) {
      yield put(actions.fetchPortfolio(response));
    }
  } catch (error) {
    console.log("/portfolio", error);
  }
}

function* fetchArticleSaga() {
  try {
    const id = yield select(selectors.selectId);
    const state = yield select();

    const response = yield call(xhr.get, `/blog/article/${id}`);
    console.log("fetchArticleSaga", response);
    if (response) {
      yield put(actions.articleRequestSuccess(response));
    }
  } catch (error) {
    console.log("/fetchArticleSaga", error);
  }
}

// function* addEventRequestSaga(action) {
//   try {
//     const response = yield call(xhr.post, "/api/v001/events", action.event);

//     console.log(response);
//     if (response) {
//       yield put(actions.addEventSuccess(response.status));
//       alert("Event added");
//       yield put(push("/"));
//     }
//   } catch (error) {
//     console.log("/postEvents", "error");
//   }
// }

export function* contentRootSaga() {
  yield all([
    yield takeLatest(types.FETCH_POTRFOLIO_REQUEST, fetchPortfolioSaga),
    yield takeLatest(types.FETCH_BLOG_REQUEST, fetchBlogSaga),
    yield takeLatest(types.FETCH_ARTICLE_REQUEST, fetchArticleSaga)
  ]);
}
