import * as types from "./ActionTypes";

export const changeContent = requestContent => ({
  type: types.CHANGE_CONTENT,
  requestContent
});

export const fetchPortfolioRequest = () => ({
  type: types.FETCH_POTRFOLIO_REQUEST
})

export const fetchPortfolio = portfolio => ({
  type: types.FETCH_POTRFOLIO,
  portfolio
})

export const fetchBlogRequest = () => ({
  type: types.FETCH_BLOG_REQUEST
})

export const fetchBlog = blog => ({
  type: types.FETCH_BLOG,
  blog
})

export const fetchArticleRequest = (articleId) => ({
  type: types.FETCH_ARTICLE_REQUEST,
  articleId
})

export const articleRequestSuccess = (article) => ({
  type: types.ARTICLE_REQUEST_SUCCESS,
  article
})
// export const fetchTokenSuccess = requestToken => ({
//   type: types.FETCH_TOKEN_SUCCESS,
//   requestToken
// });

// export const fetchTokenError = error => ({
//   type: types.FETCH_TOKEN_ERROR,
//   error
// });

// export const loginRequest = (username, password, requestToken) => ({
//   type: types.LOGIN_REQUEST,
//   username,
//   password,
//   requestToken
// });

// export const loginSuccess = username => ({
//   type: types.LOGIN_SUCCESS,
//   username
// });

// export const loginError = error => ({
//   type: types.LOGIN_ERROR,
//   error
// });

// export const logOut = () => ({
//   type: types.LOGOUT
// });
