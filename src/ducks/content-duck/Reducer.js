import * as types from "./ActionTypes";

const initialState = {
  content: "portfolio",
  articles: [],
  portfolio: [],
  article: "",
  articleId: ""
};

export const content = (state = initialState, action) => {
  switch (action.type) {
    case types.CHANGE_CONTENT:
      return {
        ...state,
        content: action.requestContent
      };
    case types.FETCH_POTRFOLIO:
      return {
        ...state,
        portfolio: action.portfolio
      };
    case types.FETCH_BLOG:
      return {
        ...state,
        articles: action.blog
      };
    case types.FETCH_ARTICLE_REQUEST:
      return {
        ...state,
        articleId: action.articleId
      }
    case types.ARTICLE_REQUEST_SUCCESS:
      return {
        ...state,
        article: action.article
      }
    default:
      return state;
  }
};
