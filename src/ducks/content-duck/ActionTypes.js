export const CHANGE_CONTENT = "@@content/CHANGE_CONTENT";
export const FETCH_POTRFOLIO_REQUEST = "@@content/FETCH_POTRFOLIO_REQUEST";
export const FETCH_POTRFOLIO = "@@content/FETCH_POTRFOLIO";
export const FETCH_BLOG_REQUEST = "@@content/FETCH_BLOG_REQUEST";
export const FETCH_BLOG = "@@content/FETCH_BLOG";
export const FETCH_ARTICLE_REQUEST = "@@content/FETCH_ARTICLE";
export const ARTICLE_REQUEST_SUCCESS = "@@content/ARTICLE_REQUEST_SUCCESS"