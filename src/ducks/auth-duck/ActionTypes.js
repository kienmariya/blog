export const AUTH_REQUEST = "@@auth/AUTH_REQUEST";
export const AUTH_SUCCESS = "@@auth/AUTH_SUCCESS";
export const LOGOUT = "@@auth/LOGOUT";
