import * as types from "./ActionTypes";

export const authRequest = (login, password) => ({
  type: types.AUTH_REQUEST,
  login,
  password
});

export const authSuccess = isAuth => ({
  type: types.AUTH_SUCCESS,
  isAuth
});

export const logout = () => ({
  type: types.LOGOUT
});
