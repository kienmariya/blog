/**
 * Пост запрос на аутентификацию
 * возвращение тру или фолс
 */

import { put, takeLatest, all, call, select } from "redux-saga/effects";
import { push } from "react-router-redux";
import XHRProvider from "../../XHRProvider";
import * as types from "./ActionTypes";
import * as actions from "./Actions";
import * as selectors from "./Selectors";
const xhr = new XHRProvider();

function* authRequestSaga(action) {
  try {
    const response = yield call(xhr.post, "/auth", {
      login: action.login,
      password: action.password
    });

    if (response) {
      yield put(actions.authSuccess(response.data));
      yield put(push("/addarticle"));
    }
  } catch (error) {
    console.log("/postEvents", "error");
  }
}

export function* authRootSaga() {
  yield all([yield takeLatest(types.AUTH_REQUEST, authRequestSaga)]);
}
