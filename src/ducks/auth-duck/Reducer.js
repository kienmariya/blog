import * as types from "./ActionTypes";

const initialState = {
  isAuth: false
};

export const auth = (state = initialState, action) => {
  switch (action.type) {
    case types.AUTH_SUCCESS:
      return {
        ...state,
        isAuth: action.isAuth
      };
    case types.LOGOUT:
      return {
        ...state,
        isAuth: false
      };
    default:
      return state;
  }
};
