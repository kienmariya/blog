import React from "react";
import ArticlePreview from "./ArticlePreview";
const ArticlesList = ({ articles }) => {
  return articles.map(el => <ArticlePreview key={el.id} article={el} />);
};

export default ArticlesList;
