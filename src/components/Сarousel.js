import React from "react";

import ModalImg from "../style/ModalWindow/ModalImg";
import CarouselContainer from "../style/ModalWindow/CarouselContainer";
import LeftArrow from "../style/ModalWindow/LeftArrow";
import RightArrow from "../style/ModalWindow/RightArrow";

class Сarousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentImgIndex: 0
    };
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  setImgRef = node => {
    this.imgRef = node;
  };
  setLeftArrowRef = node => {
    this.leftArrowRef = node;
  };
  setRightArrowRef = node => {
    this.rightArrowRef = node;
  };
  handleClickOutside(event) {
    console.log("handleClickOutside");
    if (this.props.img.length > 1) {
      if (
        this.imgRef &&
        !this.imgRef.contains(event.target) &&
        this.props.focus &&
        (this.leftArrowRef &&
          !this.leftArrowRef.contains(event.target) &&
          this.props.focus) &&
        (this.rightArrowRef &&
          !this.rightArrowRef.contains(event.target) &&
          this.props.focus)
      ) {
        console.log("done");
        this.props.toggleModalActive();
        this.props.handleClick(false);
      }
    } else {
      if (
        this.imgRef &&
        !this.imgRef.contains(event.target) &&
        this.props.focus
      ) {
        console.log("done");
        this.props.toggleModalActive();
        this.props.handleClick(false);
      }
    }
  }

  nextButtonClick = () => {
    this.state.currentImgIndex < this.props.img.length - 1
      ? this.setState((prevState, props) => {
          return { currentImgIndex: prevState.currentImgIndex + 1 };
        })
      : this.setState({ currentImgIndex: 0 });
  };

  prevButtonClick = () => {
    this.state.currentImgIndex > 0
      ? this.setState((prevState, props) => {
          return { currentImgIndex: prevState.currentImgIndex - 1 };
        })
      : this.setState({ currentImgIndex: this.props.img.length - 1 });
  };

  render() {
    return (
      <CarouselContainer>
        {this.props.img.length > 1 && (
          <LeftArrow
            ref={this.setLeftArrowRef}
            onClick={this.prevButtonClick}
          />
        )}
        <ModalImg
          ref={this.setImgRef}
          src={this.props.img[this.state.currentImgIndex]}
        />
        {this.props.img.length > 1 && (
          <RightArrow
            ref={this.setRightArrowRef}
            onClick={this.nextButtonClick}
          />
        )}
      </CarouselContainer>
    );
  }
}

export default Сarousel;
