import React from "react";

import ModalContainer from "../style/ModalWindow/ModalContainer";
import ModalContent from "../style/ModalWindow/ModalContent";

import Carousel from "../components/Сarousel";

class ModalWindow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      focus: false
    };
    // this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  // componentDidMount() {
  //   document.addEventListener("mousedown", this.handleClickOutside);
  // }

  // componentWillUnmount() {
  //   document.removeEventListener("mousedown", this.handleClickOutside);
  // }

  componentDidUpdate(prevProps) {
    if (this.props.isClick !== prevProps.isClick) {
      this.setState({ focus: this.props.isClick });
    }
  }

  toggleModalActive = () => {
    this.setState({ focus: false });
  };
  // handleClickOutside(event) {
  //   if (
  //     this.wrapperRef &&
  //     !this.wrapperRef.contains(event.target) &&
  //     this.state.focus
  //   ) {
  //     this.setState({ focus: false });
  //     this.props.handleClick(false);
  //   }
  // }

  // setWrapperRef = node => {
  //   this.wrapperRef = node;
  // };
  render() {
    return (
      <ModalContainer isClick={this.state.focus}>
        <ModalContent>
          <Carousel
            img={this.props.img}
            toggleModalActive={this.toggleModalActive}
            focus={this.state.focus}
            handleClick={this.props.handleClick}
          />
        </ModalContent>
      </ModalContainer>
    );
  }
}

export default ModalWindow;
