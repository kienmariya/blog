import React from "react";
import { Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import * as actions from "../ducks/content-duck/Actions";
import * as selectors from "../ducks/content-duck/Selectors";

import * as authActions from "../ducks/auth-duck/Actions";
import * as authSelectors from "../ducks/auth-duck/Selectors";

import HeaderBar from "../style/HeaderBar";
import HeaderTextWrap from "../style/HeaderTextWrap";
import HeaderText from "../style/HeaderText";
import HeaderContainer from "../style/HeaderContainer";
import LoginButtonContainer from "../style/LoginButtonContainer";
import Box from "../style/Box";
import LogOn from "../style/LogOn";

import Login from "../../assets/login.svg";
import Logout from "../../assets/logout.svg";
import SVG from "react-svg-inline";
class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectItem: 1
    };
  }
  handleClick = value => {
    this.props.history.push("/");
    this.setState({ selectItem: value });
    value === 1
      ? this.props.changeContent("portfolio")
      : this.props.changeContent("blog");
  };
  handleLoginButoon = () => {
    this.props.history.push("/auth");
  };
  handleLogoutButton = () => {
    this.props.history.push("/");
    this.props.logout();
  };
  render() {
    return (
      <HeaderBar>
        <HeaderContainer>
          <Box>
            <HeaderTextWrap selectItem={this.state.selectItem}>
              <HeaderText onClick={() => this.handleClick(1)}>
                Портфолио
              </HeaderText>
            </HeaderTextWrap>
            <HeaderTextWrap selectItem={this.state.selectItem}>
              <HeaderText onClick={() => this.handleClick(2)}>Блог</HeaderText>
            </HeaderTextWrap>
          </Box>
          <LoginButtonContainer>
            {this.props.isAuth ? (
              <LogOn onClick={this.handleLogoutButton}>
                {/* <p>logout</p> */}
                {/* <SVG width="40px" svg={KeyImage} /> */}
                <SVG width="40px" svg={Logout} />
              </LogOn>
            ) : (
              <LogOn onClick={this.handleLoginButoon}>
                <SVG width="40px" svg={Login} />
              </LogOn>
            )}
          </LoginButtonContainer>
        </HeaderContainer>
      </HeaderBar>
    );
  }
}

const mapStateToProps = state => ({
  content: selectors.selectContent(state),
  isAuth: authSelectors.selectIsAuth(state)
});

const mapDispatchToProps = {
  changeContent: actions.changeContent,
  logout: authActions.logout
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Header));
