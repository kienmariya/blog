import React from "react";

const AddArticle = (props) => {
  return (
    <div>
      <form onSubmit={props.handleSubmit} action="">
        <label>Название</label>
        <input name="articleName" onChange={props.handleChange} type="text" />
        <label>Тело</label>
        <textarea name="articleBody" onChange={props.handleChange} name="" id="" cols="30" rows="10"></textarea>
        <button onClick={props.handleSubmit}>Добавить статью</button>
      </form>
    </div>
  );
}


export default AddArticle;
