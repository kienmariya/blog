import React from "react";
import { withRouter } from "react-router-dom";

const ArticlesList = ({ article: { id, name, partOfAtricle, date }, history }) => {
    const showMore = () => {
        history.push(`article/${id}`)
    }
    return <div>
        <h1 onClick={showMore}>{name}</h1>
        <p>{partOfAtricle}</p>
        <p>{date}</p>
    </div>
}

export default withRouter(ArticlesList);
