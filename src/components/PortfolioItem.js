import React from "react";

import ModalWindow from "./ModalWindow";

import PortfolioItemContainer from "../style/PortfolioItemContainer";
import PortfolioItemDescr from "../style/PortfolioItemDescr";
import PortfolioItemText from "../style/PortfolioItemText";
import PortfolioItemImg from "../style/PortfolioItemImg";
import PortfolioItemTitle from "../style/PortfolioItemTitle";

class PortfolioItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      click: false
    };
  }

  handleClick = isClick => {
    console.log(isClick);
    this.setState({ click: isClick });
  };
  render() {
    return (
      <PortfolioItemContainer>
        <PortfolioItemDescr>
          <PortfolioItemTitle onClick={() => this.handleClick(true)}>
            {this.props.data.name}
          </PortfolioItemTitle>
          <PortfolioItemText>{this.props.data.description}</PortfolioItemText>
          <p>Cтек технологий: {this.props.data.technology}</p>
        </PortfolioItemDescr>
        <PortfolioItemImg
          onClick={() => this.handleClick(true)}
          src={this.props.data.img[0]}
        />
        <ModalWindow
          handleClick={this.handleClick}
          isClick={this.state.click}
          img={this.props.data.img}
        />
      </PortfolioItemContainer>
    );
  }
}

export default PortfolioItem;
